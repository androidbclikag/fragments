package com.example.fragmentsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.fragmentsapp.fragments.SignInFragment
import com.example.fragmentsapp.fragments.SignUpFragment
import kotlinx.android.synthetic.main.activity_main.*

open class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {

        signInButtonBase.setOnClickListener {
            addFragment(SignInFragment(), R.id.fragment, "signInFragment")
        }


        signUpButtonBase.setOnClickListener {
            addFragment(SignUpFragment(), R.id.fragment, "signUpFragment")

        }

    }

    private fun addFragment(fragment: Fragment, container: Int, tag: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(container, fragment, tag)
        transaction.addToBackStack(tag)
        transaction.commit()

    }

}
