package com.example.fragmentsapp.api

interface CallBack {

    fun onResponse( response: String) {}

    fun onFailure(body: String) {}

    fun onError(body: String, message: String) {}

}