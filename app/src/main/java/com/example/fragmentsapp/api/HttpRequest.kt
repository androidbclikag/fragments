package com.example.fragmentsapp.api

import android.util.Log.d
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Path

object HttpRequest {


    const val LOGIN = "login"
    const val REGISTER = "register"
    const val HTTP_STATUS_CODE_OK_200 = 200
    const val HTTP_STATUS_CODE_CREATED_201 = 201
    const val HTTP_BAD_REQUEST_400 = 400
    const val HTTP_UNAUTHORIZED_401 = 401




    private var retrofit = Retrofit.Builder().addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://reqres.in/api/")
        .build()

    private var service = retrofit.create(
        ApiService::class.java
    )

    fun postRequest(path: String, parameters: MutableMap<String, String>, callback: CallBack) {
        val call = service.postRequest(path, parameters)
        call.enqueue(
            onCallBackPost(
                callback,
                parameters
            )
        )
    }

    private fun onCallBackPost(callback: CallBack, parameters: MutableMap<String, String>) =
        object : Callback<String> {
            override fun onFailure(call: retrofit2.Call<String>, t: Throwable) {
                callback.onFailure(t.message.toString())
            }

            override fun onResponse(call: retrofit2.Call<String>, response: Response<String>) {

                val statusCode = response.code()
                if (statusCode == HTTP_STATUS_CODE_CREATED_201 || statusCode == HTTP_STATUS_CODE_OK_200) {
                    callback.onResponse(response.body().toString())
                    d("success", "$statusCode")
                } else if (statusCode == HTTP_BAD_REQUEST_400 || statusCode == HTTP_UNAUTHORIZED_401) {
                    try {
                        val json = JSONObject(response.errorBody()!!.string())
                        if (parameters != null) {
                            parameters.forEach {
                                if (json.has(it.key))
                                    json.getString(it.key)
                            }
                        }
                        if (json.has("error"))
                            callback.onFailure(json.getString("error"))
                        d("error", "${response.errorBody()}")
                        callback.onError(
                            response.errorBody().toString(),
                            json.getString("error")
                        )


                    } catch (e: JSONException) {

                    }
                }

            }

            }


    interface ApiService {

        @FormUrlEncoded
        @POST("{path}")
        fun postRequest(
            @Path("path") path: String, @FieldMap parameters: Map<String, String?>): retrofit2.Call<String>

    }
}