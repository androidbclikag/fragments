package com.example.fragmentsapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.fragmentsapp.MainActivity
import com.example.fragmentsapp.R
import com.example.fragmentsapp.api.CallBack
import com.example.fragmentsapp.api.HttpRequest
import kotlinx.android.synthetic.main.fragment_sign_in.view.*

class SignInFragment : Fragment() {

    private lateinit var itemView: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        itemView = inflater.inflate(R.layout.fragment_sign_in, container, false)
        init()
        return itemView
    }

    private fun init() {
        itemView.signInButton.setOnClickListener { logIn() }

    }


    private fun logIn() {
        val parameters = mutableMapOf<String, String>()
        val email = itemView.emailEditText.text.toString()
        val password = itemView.passwordEditText.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty()) {
            parameters["email"] = email
            parameters["password"] = password
            HttpRequest.postRequest(
                HttpRequest.LOGIN,
                parameters,
                object : CallBack {
                    override fun onFailure(body: String) {

                        Toast.makeText(activity as MainActivity, "fail", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(response: String) {

                        Toast.makeText(activity as MainActivity, "Success", Toast.LENGTH_SHORT)
                            .show()


                    }

                    override fun onError(body: String, message: String) {
                        Toast.makeText(
                            activity as MainActivity,
                            "${body}, $message",
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                })
        } else
            Toast.makeText(activity as MainActivity, "please fill all fields", Toast.LENGTH_SHORT)
                .show()

    }


}
