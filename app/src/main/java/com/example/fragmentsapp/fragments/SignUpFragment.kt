package com.example.fragmentsapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.fragmentsapp.MainActivity
import com.example.fragmentsapp.R
import com.example.fragmentsapp.api.CallBack
import com.example.fragmentsapp.api.HttpRequest
import kotlinx.android.synthetic.main.fragment_sign_up.view.*
import kotlinx.android.synthetic.main.fragment_sign_up.view.confirmPasswordEditTextSU
import kotlinx.android.synthetic.main.fragment_sign_up.view.signUpButton


class SignUpFragment : Fragment() {

    private lateinit var itemView: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        itemView = inflater.inflate(R.layout.fragment_sign_up, container, false)
        init()
        return itemView

    }

    private fun init() {
        itemView.signUpButton.setOnClickListener { register() }
    }


    private fun register() {
        val parameters = mutableMapOf<String, String>()
        val email = itemView.emailEditTextSU.text.toString()
        val password = itemView.passwordEditTextSU.text.toString()
        val confirm = itemView.confirmPasswordEditTextSU.text.toString()
        if (password == confirm) {

            parameters["email"] = email
            parameters["password"] = password


            HttpRequest.postRequest(
                HttpRequest.REGISTER,
                parameters,
                object : CallBack {
                    override fun onResponse(response: String) {
                        Toast.makeText(activity as MainActivity, "Success", Toast.LENGTH_SHORT)
                            .show()

                    }

                    override fun onFailure(body: String) {
                        Toast.makeText(activity as MainActivity, "fail", Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(body: String, message: String) {
                        itemView.errorTextView.text = message
                        Toast.makeText(activity as MainActivity, body, Toast.LENGTH_SHORT).show()
                    }
                })

        } else
            Toast.makeText(
                activity as MainActivity,
                "Can`t confirm, wrong password",
                Toast.LENGTH_SHORT
            ).show()


    }

}
